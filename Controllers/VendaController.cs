using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
      private readonly VendedoresContext _context;

        public VendaController(VendedoresContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            // TODO: Buscar o Id no banco utilizando o EF
            // TODO: Validar o tipo de retorno. Se não encontrar a tarefa, retornar NotFound,
            // caso contrário retornar OK com a tarefa encontrada
            var vendas = _context.Vendas.Find(id);

            if (vendas == null)
                return NotFound();


            return Ok(vendas);
        }
              
        [HttpPost("Criar Vendedor")]
        public IActionResult Criar(Vendedor vendedor)
        {
            
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { Id = vendedor.Id }, vendedor);
        }

        [HttpPost("Criar Produto")]
        public IActionResult Criar(Produto produto)
        {
            
            _context.Add(produto);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { Id = produto.Id }, produto);
        }

        [HttpPost("Criar Venda")]
        public IActionResult Criar(Venda venda)
        {
            // if (venda.ProdutoId == null)
            //   return BadRequest(new { Erro = "A venda deve possuir pelo menos um item" });
            
            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { Id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if(vendaBanco.Status == EnumStatusVenda.AguardandoPagamento 
                && venda.Status != EnumStatusVenda.PagamentoAprovado || 
                venda.Status != EnumStatusVenda.Cancelada)

                return BadRequest(new { Erro = "Venda só pode ser atualizada para Aprovada ou Cancelada" });
            
            if(vendaBanco.Status == EnumStatusVenda.PagamentoAprovado 
                && venda.Status != EnumStatusVenda.EnviadoTransportadora || 
                venda.Status != EnumStatusVenda.Cancelada)

                return BadRequest(new { Erro = "Venda só pode ser atualizada para Cancelada ou Enviada" });

            if(vendaBanco.Status == EnumStatusVenda.EnviadoTransportadora 
                && venda.Status != EnumStatusVenda.Entregue)

                return BadRequest(new { Erro = "Venda só pode ser atualizada para Entregue" });
            
            
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

        

    }
}