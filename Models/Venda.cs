using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }

        public decimal Valor { get; set;}

        public DateTime Data { get; set; }

        public EnumStatusVenda Status {
            get{
                return Status;
            }
            
            set{
                value = 0;
            }
        }

        //public int VendedorId { get; set; }
        public virtual Vendedor Vendedor { get; set; }

        //public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }

    }

}
